module github.com/dessaya/autocommit

go 1.14

require (
	github.com/go-git/go-git/v5 v5.1.0
	github.com/radovskyb/watcher v1.0.7
)

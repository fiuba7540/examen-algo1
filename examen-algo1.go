package main

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/radovskyb/watcher"
)

const basePath = "./examen"

func check(err error) {
	if err != nil {
		fmt.Printf("error: %s", err)
		os.Exit(1)
	}
}

var _w = bufio.NewWriter(os.Stdout)

func out(msg string) {
	_w.Write([]byte(msg))
	_w.Flush()
}

func input(msg string) string {
	reader := bufio.NewReader(os.Stdin)
	out(msg + ": ")
	s, err := reader.ReadString('\n')
	check(err)
	return s
}

func main() {
	if len(os.Args) != 1 && len(os.Args) != 2 {
		fmt.Printf("Usage: %s [remote-path]\n", os.Args[0])
		os.Exit(1)
	}

	r := openRepo()
	startWatcher(r)

	out("\n")
	out(fmt.Sprintf("Todo listo. Los archivos del examen están en la carpeta '%s'.\n", basePath))
	out("Importante: Dejá este programa funcionando mientras desarrollás el examen.\n")
	out("Presiona CTRL+C cuando quieras finalizar.\n")
	out("¡Éxitos!\n")

	waitForCtrlC()
}

func waitForCtrlC() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
	out("\n")
	out(":)\n")
}

func repoExists() bool {
	fi, err := os.Stat(basePath)

	switch {
	case err != nil:
		// handle the error and return
		if os.IsNotExist(err) {
			return false
		}
		check(err)
	case fi.IsDir():
		return true
	default:
		check(fmt.Errorf("'%s' debería ser un directorio", basePath))
	}
	return false
}

func getRemotePath() string {
	if len(os.Args) == 2 {
		return os.Args[1]
	}
	return input("URL del examen")
}

func openRepo() *git.Repository {
	if repoExists() {
		out("Parece que los archivos del examen ya fueron descargados.\n")
		r, err := git.PlainOpen(basePath)
		check(err)
		return r
	}
	remote := getRemotePath()
	out("Descargando los archivos del examen... ")
	r, err := git.PlainClone(basePath, false, &git.CloneOptions{
		URL: remote,
	})
	out("Listo.\n")
	check(err)
	return r
}

func startWatcher(r *git.Repository) {
	w := watcher.New()
	w.SetMaxEvents(1)
	w.IgnoreHiddenFiles(true)
	err := w.AddRecursive(basePath)
	check(err)

	go func() {
		err = w.Start(1 * time.Second)
		check(err)
	}()

	go func() {
		for {
			select {
			case <-w.Event:
				w, err := r.Worktree()
				check(err)
				_, err = w.Add(".")
				check(err)
				_, err = w.Commit("Cambio", &git.CommitOptions{
					All: true,
					Author: &object.Signature{
						Name:  "examen",
						Email: "examen@examen.com",
						When:  time.Now(),
					},
				})
				check(err)
			case err := <-w.Error:
				check(err)
			case <-w.Closed:
				return
			}
		}
	}()

	w.Wait()
}
